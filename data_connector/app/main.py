"""
    Performs data exploration on provided dataset
"""

import json
import os
import time

import click
from xpresso.ai.client.data_client import config
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException, CLICommandFailedException, CreateBranchException
from xpresso.ai.core.commons.utils.constants import KEY_RUN_NAME, \
    COMPONENT_NAME_KEY, PROJECT_NAME_KEY
from xpresso.ai.core.commons.utils.generic_utils import move_directory
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.automl.unstructured_dataset import UnstructuredDataset
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from datetime import datetime
from copy import deepcopy


__all__ = ["DataConnector"]
__author__ = ["Ashritha Goramane", "Gopi Krishna Kanugula"]


class DataConnector(AbstractPipelineComponent):
    """
        To import data from different source using data connector
    """
    DATASET_TYPE = 'dataset_type'
    DATA_CONFIG = "data_config"

    OUT_PATH = "out_path"
    ENV_ARGUMENT = "environment"
    OPTIONS = "options"
    DATA_CONFIG_TYPE = "data_config_type"
    DATA_CONFIG_PATH = "data_config_path"
    DATA_CONFIG_DATA_SOURCE = "data_config_data_source"
    DATA_CONFIG_TABLE = "data_config_table"
    DATA_CONFIG_COLUMNS = "data_config_columns"
    DATA_CONFIG_DSN = "data_config_dsn"
    FILE_NAME = "file_name"

    def __init__(self, **kwargs):
        super().__init__(name="data_connector")
        self.name = "data_connector"
        self.cli_args = {}
        self.arguments = kwargs
        self.fetch_arguments()
        self.logger = XprLogger()
        self.xpr_con = None
        self.config = config
        self.dataset = None
        self.explorer = None
        self.start_timestamp = None
        self.end_timestamp = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data
        preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the
          Controller that
              the component has started processing (details such as
              the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
                identify the current run. It must be passed. While running as
                pipeline,
               Xpresso automatically adds it.
        """
        super().start(xpresso_run_name=xpresso_run_name)
        self.start_timestamp = time.time()
        self.update_iq_data_config(self.arguments)
        print("Data connector component starting", flush=True)
        self.import_dataset(self.arguments["data_config_1"], self.arguments["dataset_1_type"])
        self.import_dataset(self.arguments["data_config_2"], self.arguments["dataset_2_type"])
        self.end_timestamp = time.time()
        print("Data connector component completed", flush=True)
        self.send_metrics("import_dataset")
        commit_id, file_path = self.save_new_data()
        self.save_commit_info_to_config(commit_id, file_path)
        self.completed()
        print("successful")

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        super().completed(push_exp=push_exp)

    def import_dataset(self, data_config=None, dataset_type=None):
        """Import dataset as structured or unstructured using data connector"""
        print("Importing dataset", flush=True)
        self.get_dataset_object(dataset_type)
        updated_data_config = self.set_data_config(data_config)
        self.logger.info(
            f"Importing dataset with {updated_data_config}")
        self.dataset.import_dataset(updated_data_config)
        try:
            if self.dataset.data.empty:
                print("Unable to import dataset with given config")
        except AttributeError:
            print("Given file path doesn't exists. Unable to import dataset with given "
                  "config")
        self.save_dataset(data_config)

    def set_data_config(self, data_config=None):
        """Set data config from arguments"""
        if data_config and len(data_config):
            return data_config
        if self.cli_args[self.DATA_CONFIG]:
            return deepcopy(self.cli_args[self.DATA_CONFIG])
        data_config = dict()
        config_arguments = {self.DATA_CONFIG_TYPE: "type",
                            self.DATA_CONFIG_DATA_SOURCE: "data_source",
                            self.DATA_CONFIG_PATH: "path",
                            self.DATA_CONFIG_TABLE: "table",
                            self.DATA_CONFIG_COLUMNS: "columns",
                            self.DATA_CONFIG_DSN: "DSN"}
        print(self.cli_args[self.DATA_CONFIG_DSN])
        for key, value in config_arguments.items():
            if self.cli_args[key]:
                data_config[value] = self.cli_args[key]
        self.cli_args[self.DATA_CONFIG] = data_config
        return deepcopy(data_config)

    def get_dataset_object(self, dataset_type=None):
        """Depending upon dataset type get specific dataset object"""
        if not dataset_type:
            dataset_type = self.cli_args[self.DATASET_TYPE]
        print(dataset_type)
        if dataset_type == DatasetType.STRUCTURED.value:
            self.dataset = StructuredDataset()
        elif dataset_type == DatasetType.UTEXT.value:
            self.dataset = UnstructuredDataset()
        else:
            raise InvalidDatatypeException(f"Dataset type {dataset_type} not "
                                           "supported")

    def send_metrics(self, status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": status},
            "metric": {
                "elapsed_time": self.end_timestamp - self.start_timestamp}
        }
        self.report_status(status=report_status)

    def save_dataset(self, data_config=None):
        """Save dataset into out path"""
        # folder_path = self.dataset.save()
        if not data_config:
            data_config = self.cli_args[self.DATA_CONFIG]
        # self.logger.info(
        #     f"Saving dataset to {self.cli_args[self.OUT_PATH]}")
        # move_directory(folder_path, self.cli_args[self.OUT_PATH])
        # print(f"Data saved to {self.cli_args[self.OUT_PATH]}")
        file_name = os.path.basename(data_config["path"])
        if file_name:
            output_path = os.path.join(
                self.cli_args[self.OUT_PATH],file_name)
            if not os.path.exists(self.cli_args[self.OUT_PATH]):
                os.makedirs(self.cli_args[self.OUT_PATH])
            print(f"Saving data csv into {output_path}")
            self.dataset.data.to_csv(output_path, index=False)

    def extract_argument(self, argument):
        """
        Args:
            argument(str): Name of argument to extract
        Returns:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        Returns:
            Returns arguments
        """
        arguments_key = [self.DATASET_TYPE, self.DATA_CONFIG_PATH,
                         self.OUT_PATH, self.DATA_CONFIG_TYPE,
                         self.DATA_CONFIG_DATA_SOURCE, self.DATA_CONFIG_DSN,
                         self.DATA_CONFIG_TABLE, self.DATA_CONFIG_COLUMNS,
                         self.DATA_CONFIG, self.OPTIONS, self.FILE_NAME]

        for arg in arguments_key:
            self.cli_args[arg] = self.extract_argument(arg)

        try:
            if self.cli_args[self.DATA_CONFIG]:
                self.cli_args[self.DATA_CONFIG] = json.loads(
                    self.cli_args[self.DATA_CONFIG])
        except json.JSONDecodeError as e:
            raise CLICommandFailedException(
                f"Invalid JSON file provided as data_config. \n"
                f"Error on line number {e.lineno} : "
                f"'{e.msg}'.\n"
                f"Please check your file's formatting.")

        try:
            if self.cli_args[self.OPTIONS]:
                self.cli_args[self.OPTIONS] = json.loads(
                    self.cli_args[self.OPTIONS])
        except json.JSONDecodeError as e:
            raise CLICommandFailedException(
                f"Invalid JSON file provided as options. \n"
                f"Error on line number {e.lineno} : "
                f"'{e.msg}'.\n"
                f"Please check your file's formatting.")

    def save_new_data(self):
        """
        saves the new data in data versioning system
        """
        xpresso_run_name = self.arguments[KEY_RUN_NAME]
        run_info = \
            self.controller.api_utils.decode_xpresso_run_name(xpresso_run_name)
        project_name = run_info["project_name"]
        #project_name = "insights_quotient"
        version_controller = \
            self.controller.fetch_version_controller(project_name)
        branch_name = "iq_covid_data"
        try:
            version_controller.create_branch(
                **{
                    "repo_name": project_name,
                    "branch_name": branch_name
                })
        except CreateBranchException:
            print("Branch already present")
        dataset_name = datetime.now().strftime("%B_%d_%Y")
        push_input = {
                "repo_name": project_name,
                "branch_name": branch_name,
                "description": "Pushing updated iq covid data",
                "data_type": "files",
                "path": self.cli_args[self.OUT_PATH],
                "dataset_name": dataset_name
        }
        commit_id, file_path_on_dv = \
            version_controller.push_dataset(**push_input)
        return commit_id, file_path_on_dv

    def save_commit_info_to_config(self, commit_id, file_path_on_dv):
        """
        save the information of commit_id of the versioned training data
        """
        config_path = "/data/config/config.json"
        if os.path.exists(os.path.dirname(config_path)):
            with open(config_path) as config_file:
                try:
                    current_config_info = json.load(config_file)
                except json.JSONDecodeError as err:
                    print(err)
                    current_config_info = dict()
        else:
            os.makedirs(os.path.dirname(config_path))
            current_config_info = dict()
        current_config_info["covid_data_info"] = {
            "commit_id": commit_id,
            "data_path_on_dv": file_path_on_dv
        }
        with open(config_path, "w") as config_file:
            json.dump(current_config_info, config_file)

    def update_iq_data_config(self, kwargs):
        """
        Check the input config provided and
        update the file paths & config in case not provided
        """
        if "data_config_1" not in kwargs:
            kwargs["data_config_1"] = {
                "type": "FS",
                "options": {"sep": ","},
                "path": "/covid_19.db/covid_cases/covid-19.csv"
            }
        if "dataset_1_type" not in kwargs:
            kwargs["dataset_1_type"] = "structured"
        if "data_config_2" not in kwargs:
            kwargs["data_config_2"] = {
                "type": "FS",
                "options": {"sep": ","},
                "path": "/covid_19.db/us_covid_cases/us_covid_cases.csv"
            }
        if "dataset_2_type" not in kwargs:
            kwargs["dataset_2_type"] = "structured"
        if self.OUT_PATH in kwargs and not kwargs[self.OUT_PATH]:
            self.cli_args[self.OUT_PATH] = datetime.now().strftime("%B_%d_%Y")


@click.command()
@click.argument(KEY_RUN_NAME)
#@click.argument(COMPONENT_NAME_KEY)
@click.option('-out-path', type=str, help='Path of the file to load data from')
@click.option('-data-config-type', type=str, help='Source where to pick data '
                                                  'from')
@click.option('-data-config-path', type=str,
              help='Path of the file to load data from')
@click.option('-data-config-dsn', type=str,
              help='Data Source Name. For reference refer to '
                   'Data Source Connectivity document'
              )
@click.option('-data-config-data-source', type=str,
              help='Data source of the file to load '
                   'data from')
@click.option('-data-config-table', type=str,
              help='Name of the table in presto data source')
@click.option('-data-config-columns', type=str,
              help='Name of column inside the table '
                   'stored in presto data source')
@click.option('-data-config', type=str,
              help='Data configuration to load data from Eg. -i \'{"uid": '
                   '"abc"}\')')
@click.option('-data-config-options', type=str,
              help='Extra keyword arguments to be specified as key-value pair,'
                   ' for better importing through files. Eg.\'{"type":"FS", '
                   '"path":"/test_connector/small_size_data/test.csv"}\'')
@click.option('-dataset-type', type=str, help='Dataset type (i.e structured, '
                                              'unstructured)')
@click.option('-file-name', type=str, help='File name for the csv file')
def cli_options(**kwargs):
    print(kwargs)
    result = DataConnector(**kwargs)
    try:
        if KEY_RUN_NAME in kwargs:
            result.start(xpresso_run_name=kwargs[KEY_RUN_NAME])
        else:
            result.start(xpresso_run_name="")
    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    print("entered data connector call")
    cli_options()
