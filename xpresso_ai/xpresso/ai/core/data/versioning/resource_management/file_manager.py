
from os import getcwd as get_cwd, path as path_library, makedirs as make_dirs, \
    listdir as list_dir
from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.commons.exceptions.xpr_exceptions\
    import DatasetInfoException, FileNotFoundException, \
    FileExistsException, LocalFilePathException
from xpresso.ai.core.data.versioning.resource_management.commit_manager \
    import CommitManager
from xpresso.ai.core.data.versioning.utils \
    import write_to_file
from tqdm import tqdm
from xpresso.ai.core.commons.utils.generic_utils import backup_folder


class DatasetManager(PachydermResource):
    """
    Manages files and its operations on a pachyderm cluster
    """
    INPUT_REPO_NAME = "repo_name"
    INPUT_BRANCH_NAME = "branch_name"
    INPUT_COMMIT_ID = "commit_id"
    INPUT_PATH = "path"
    COMMIT_INFO_ID = "commit_id"
    COMMIT_INFO_BRANCH = "branch_name"
    DEFAULT_LIST_DATASET_PATH = "/"
    FILE_TYPE = "type"
    OUTPUT_COMMIT_FIELD = "commit"
    OUTPUT_DATASET_FIELD = "dataset"
    FILE_SIZE = "size_in_bytes"

    def __init__(self, pachyderm_client, persistence_manager):
        super().__init__()
        self.pachyderm_client = pachyderm_client
        self.commit_manager = CommitManager(pachyderm_client, persistence_manager)

    def list(self, repo_name, commit_id, path):
        """
        lists all automl on pachyderm cluster at provided path

        :param repo_name:
            name of the repo
        :param path:
            (Optional) path of the dir in which automl might be present
                       path of the automl
        :param commit_id:
            (Optional) id of the commit
        :return:
            returns the path of the directory where automl is saved
        """
        # verifies provided info and returns a dict with request input fields
        list_output = {}
        dataset_list = self.pachyderm_client.list_dataset(repo_name, commit_id, path)
        list_output[self.OUTPUT_DATASET_FIELD] = dataset_list

        commit_info = self.commit_manager.inspect(repo_name, commit_id)
        list_output[self.OUTPUT_COMMIT_FIELD] = commit_info
        return list_output

    def pull(self, repo_name, list_dataset_output, save_at_path=None):
        """
        pulls a automl from pachyderm cluster and load it locally

        :param repo_name:
            name of the repo
        :param list_dataset_output:
            output list from list_dataset
        :param save_at_path: (Optional parameter)
            path at which the data needs to be saved.
        :return:
            returns the path of the directory where automl is saved
        """
        if not len(list_dataset_output[self.OUTPUT_DATASET_FIELD]):
            raise DatasetInfoException("No automl found at this path")

        if save_at_path and len(save_at_path):
            # If local path to save the directory is provided
            try:
                # This method checks if this directory is empty or not and
                # creates a backup if it is not empty
                backup_folder(save_at_path, clear_data=True)
            except FileNotFoundException:
                # Create a new directory to save the pulled dataset temporarily
                make_dirs(save_at_path)
            # Get the absolute path to avoid any issues while writing the data
            new_dir_path = path_library.abspath(save_at_path)
        else:
            # If local path where data needs to be saved is not provided
            current_dir = get_cwd()
            # Create a new directory under the name of commit id
            # in the current working directory
            new_dir_path = path_library.join(
                current_dir,
                list_dataset_output[self.OUTPUT_COMMIT_FIELD][self.COMMIT_INFO_ID])
            if not path_library.exists(new_dir_path):
                make_dirs(new_dir_path)

        list_in_tqdm = tqdm(list_dataset_output[self.OUTPUT_DATASET_FIELD],
                            leave=False)
        for file_info in list_in_tqdm:
            if file_info[self.FILE_TYPE] == "File":
                list_in_tqdm.set_description(
                    f"fetching file: Path - {file_info[self.INPUT_PATH]};"
                    f" Size - {file_info[self.FILE_SIZE]}"
                )

                # This needs to be done because path in file_info contains pachyderm path
                # It always starts with / . Hence it needs to be excluded exclusively
                file_info[self.INPUT_PATH] = file_info[self.INPUT_PATH].lstrip("/")

                local_write_path = path_library.join(new_dir_path,
                                                     file_info[self.INPUT_PATH])
                if path_library.isfile(path_library.dirname(local_write_path)):
                    raise LocalFilePathException(
                        f"file exists at this path; {local_write_path}")

                if not path_library.exists(local_write_path) and \
                    not path_library.isdir(
                        path_library.dirname(local_write_path)):
                    make_dirs(path_library.dirname(local_write_path))

                with open(local_write_path, "wb") as out_file:
                    data_written_size = self.pachyderm_client.pull_dataset(
                        repo_name=repo_name,
                        commit_id=list_dataset_output[self.OUTPUT_COMMIT_FIELD][
                            self.COMMIT_INFO_ID],
                        path=file_info[self.INPUT_PATH],
                        file_obj=out_file
                    )
                    list_in_tqdm.set_description(
                        f"File has been pushed: {data_written_size} bytes"
                    )
        return new_dir_path

    def inspect(self, *args):
        pass

    def delete(self, repo_name, commit_id, path="/"):
        """
        deletes a automl from the pachyderm cluster

        :param repo_name:
            name of the repo automl is in
        :param path:
            (Optional)path at which automl is saved
        :param commit_id:
            id of the commit in which automl is added
        :return:
        """
        self.pachyderm_client.delete_dataset(repo_name, commit_id, path)
