from xpresso.ai.server.controller.xprobject import XprObject
from datetime import datetime
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import BranchTypeException, MissingFieldException, \
    BlankFieldException, BranchInfoException
from xpresso.ai.server.controller.persistence.db_record \
    import DBRecordObject
from copy import deepcopy


class XprDataVersioningBranch(XprObject):
    """
    A class on xpresso platform which inherits proper of XprObject
    to represent a branch in data versioning system
    """
    def __init__(self, branch_info: dict):
        super().__init__(branch_info)

        self.valid_field_types = {
            DVFields.REPO_NAME.value: str,
            DVFields.BRANCH_NAME.value: str,
            DVFields.BRANCH_TYPE.value: str,
            DVFields.BRANCH_CREATED_BY.value: str,
            DVFields.BRANCH_CREATED_ON.value: datetime.date,
            DVFields.LAST_COMMIT_ID.value: int,
            DVFields.LAST_COMMIT_BY.value: str,
            DVFields.LAST_COMMIT_ON.value: datetime.date,
            DVFields.TOTAL_FIlES_IN_BRANCH.value: int,
            DVFields.SIZE_OF_BRANCH.value: int,
            DVFields.BRANCH_COMMITS_KEY.value: list
        }

        self.default_branch_record = {
            DVFields.REPO_NAME.value: str(),
            DVFields.BRANCH_NAME.value: str(),
            DVFields.BRANCH_TYPE.value: str(),
            DVFields.BRANCH_CREATED_BY.value: str(),
            DVFields.BRANCH_CREATED_ON.value: datetime.now(),
            DVFields.LAST_COMMIT_ID.value: int(0),
            DVFields.LAST_COMMIT_BY.value: str("-"),
            DVFields.LAST_COMMIT_ON.value: str("-"),
            DVFields.TOTAL_FIlES_IN_BRANCH.value: int(0),
            DVFields.SIZE_OF_BRANCH.value: int(0),
            DVFields.BRANCH_COMMITS_KEY.value: list()
        }

        self.mandatory_fields = [
            DVFields.REPO_NAME.value,
            DVFields.BRANCH_NAME.value
        ]

        self.valid_branch_type_values = [
            DVFields.BRANCH_TYPE_DATA.value,
            DVFields.BRANCH_TYPE_MODEL.value
        ]
        self.default_branch_type = DVFields.BRANCH_TYPE_MODEL.value

    def validate_branch_type(self):
        """
        validates the branch_type field value.

        Args: None
        Returns:
            returns the branch_type if exists and is valid else returns None
        """
        if DVFields.BRANCH_TYPE.value not in self.data:
            # If branch_type is not provided
            return None
        if self.data[DVFields.BRANCH_TYPE.value] not in self.valid_branch_type_values:
            raise BranchTypeException(
                f"Invalid {DVFields.BRANCH_TYPE.value} value. Please check and try again."
            )
        return self.data[DVFields.BRANCH_TYPE.value]

    def validate_mandatory_fields(self):
        """
        validate the input to check if the mandatory fields are provided
        """
        try:
            super().validate_mandatory_fields()
        except (MissingFieldException, BlankFieldException) as err:
            raise BranchInfoException(err.message)

    def fetch_info_for_db_record(self):
        """
        Generate a dictionary with the info of the branch
        following the format of a record in branches collection
        """
        db_record = deepcopy(self.default_branch_record)
        for field in self.default_branch_record.keys():
            if field in self.data:
                db_record[field] = self.data[field]
        return DBRecordObject("branches", db_record)
