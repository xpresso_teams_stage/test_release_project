from xpresso.ai.server.controller.xprobject import XprObject
from datetime import datetime
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import BranchTypeException, MissingFieldException, \
    BlankFieldException, BranchInfoException
from copy import deepcopy


class XprDataVersioningCommit(XprObject):
    """
    A class on xpresso platform which inherits proper of XprObject
    to represent a commit of a branch in data versioning system
    """
    def __init__(self, commit_info: dict):
        super().__init__(commit_info)

        self.valid_field_types = {
            DVFields.DV_COMMIT_ID.value: str,
            DVFields.XPRESSO_COMMIT_ID.value: int,
            DVFields.DESCRIPTION.value: str,
            DVFields.DATASET_NAME.value: str,
            DVFields.COMMITTED_BY.value: str,
            DVFields.COMMITTED_ON.value: datetime.date,
            DVFields.TOTAL_FILES_IN_COMMIT.value: int,
            DVFields.SIZE_OF_COMMIT.value: int
        }

        self.default_commit_record = {
            DVFields.DV_COMMIT_ID.value: str(),
            DVFields.XPRESSO_COMMIT_ID.value: int(),
            DVFields.DESCRIPTION.value: str(),
            DVFields.DATASET_NAME.value: str(),
            DVFields.COMMITTED_BY.value: str(),
            DVFields.COMMITTED_ON.value: datetime.now(),
            DVFields.TOTAL_FILES_IN_COMMIT.value: int(0),
            DVFields.SIZE_OF_COMMIT.value: int(0)
        }

    def fetch_info_for_db_record(self):
        """
        Generate a dictionary with the info of the branch
        following the format of a record in branches collection
        """
        db_record = deepcopy(self.default_commit_record)
        for field in self.default_commit_record.keys():
            if field in self.data:
                db_record[field] = self.data[field]
        return db_record
